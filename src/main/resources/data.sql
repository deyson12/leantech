insert into position (id, name) values (1, 'Dev');
insert into position (id, name) values (2, 'Qa');

insert into person (id, name, last_name, address, cellphone, city_name) values (1, 'Deyson', 'Estrada', 'Cr 1', '+57 123', 'Medellín');
insert into person (id, name, last_name, address, cellphone, city_name) values (2, 'Fabian', 'Posada', 'Cr 2', '+57 456', 'Bogota');
insert into person (id, name, last_name, address, cellphone, city_name) values (3, 'Angee', 'Matoma', 'Cr 3', '+57 789', 'Calí');
insert into person (id, name, last_name, address, cellphone, city_name) values (4, 'Xilena', 'Urrego', 'Cr 4', '+57 321', 'Bucaramanga');

insert into employee (id, salary, person_id, position_id) values (1, 100, 1, 1);
insert into employee (id, salary, person_id, position_id) values (2, 200, 2, 1);
insert into employee (id, salary, person_id, position_id) values (3, 300, 3, 2);
insert into employee (id, salary, person_id, position_id) values (4, 400, 4, 2);
package com.leantech.project.mapper;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;

import com.leantech.project.dto.EmployeeByPositionDTO;
import com.leantech.project.dto.EmployeeDTO;
import com.leantech.project.model.Employee;

public class EmployeeMapper {
	
	@Autowired
    private ModelMapper modelMapper;
	
	public EmployeeDTO convertToDto(Employee employee) {
		EmployeeDTO emploDto = modelMapper.map(employee, EmployeeDTO.class);
		return emploDto;
	}
	
	public EmployeeByPositionDTO convertToDtoByPosition(Employee employee) {
		EmployeeByPositionDTO emploDto = modelMapper.map(employee, EmployeeByPositionDTO.class);
		return emploDto;
	}


	public Employee convertToEntity(EmployeeDTO employeeDto) {
		Employee employee = modelMapper.map(employeeDto, Employee.class);
		return employee;
	}

}

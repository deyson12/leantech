package com.leantech.project.service;

import java.util.List;

import com.leantech.project.dto.PositionListDTO;

public interface IPositionService {

	public List<PositionListDTO> listPositionwithEmployees();

}

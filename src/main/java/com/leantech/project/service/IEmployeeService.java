package com.leantech.project.service;

import java.util.List;

import com.leantech.project.dto.EmployeeDTO;

public interface IEmployeeService {
	
	public List<EmployeeDTO> listEmployees(String name, String position);

	public EmployeeDTO createEmployee(EmployeeDTO employeeDto) throws Exception;

	public EmployeeDTO updateEmployee(int id, EmployeeDTO employeeDto) throws Exception;;

	public void deleteEmployee(int id);

}

package com.leantech.project.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import com.leantech.project.dto.EmployeeDTO;
import com.leantech.project.mapper.EmployeeMapper;
import com.leantech.project.model.Employee;
import com.leantech.project.model.Person;
import com.leantech.project.model.Position;
import com.leantech.project.repository.IEmployeeRepository;
import com.leantech.project.repository.IPersonRepository;
import com.leantech.project.repository.IPositionRepository;
import com.leantech.project.util.Util;

@Service
public class EmployeeService implements IEmployeeService{

	@PersistenceContext
	private EntityManager entityManager;
	
	@Autowired
	private EmployeeMapper employeeMapper;
	
	@Autowired
	private IEmployeeRepository emploRepository;
	
	@Autowired
	private IPersonRepository personRepository;
	
	@Autowired
	private IPositionRepository positionRepository;

	public List<EmployeeDTO> listEmployees(String name, String position) {
		
		StringBuilder statement = new StringBuilder("SELECT e FROM Employee e WHERE 1=1 ");
		if(!ObjectUtils.isEmpty(name)) {
			statement.append("AND  upper(e.person.name) LIKE upper(:name) ");
		}
		if(!ObjectUtils.isEmpty(position)) {
			statement.append("AND  upper(e.position.name) LIKE upper(:position) ");
		}
		
		Query query = entityManager.createQuery(statement.toString());
		
		if(!ObjectUtils.isEmpty(name)) {
			query.setParameter("name", "%"+name+"%");
		}
		if(!ObjectUtils.isEmpty(position)) {
			query.setParameter("position", "%"+position+"%");
		}
			
		List<Employee> employees = query.getResultList();
		
		List<EmployeeDTO> result = new ArrayList<EmployeeDTO>(employees.size());

		for (Employee employee : employees) {
			EmployeeDTO employeeDto = employeeMapper.convertToDto(employee);
			result.add(employeeDto);
		}

		return result;
	}

	@Override
	public EmployeeDTO createEmployee(EmployeeDTO employeeDto) throws Exception {
		Employee employee = employeeMapper.convertToEntity(employeeDto);
		
		//Validate if person_id is not null and if it exist on DB
		Optional<Person> person = personRepository.findById(employee.getPerson().getId());
		if(person.isPresent()) {
			//If exist, we ignore the info sent on the request and use the DB data
			employee.setPerson(person.get());
		}else {
			//Else, we create a new Person
			Util.isEmpty(employee.getPerson().getName(), "person.name");
			Util.isEmpty(employee.getPerson().getLastName(), "person.lastName");
			Util.isEmpty(employee.getPerson().getAddress(), "person.address");
			Util.isEmpty(employee.getPerson().getCellphone(), "person.cellphone");
			Util.isEmpty(employee.getPerson().getCityName(), "person.cityName");
			
			employee.setPerson(personRepository.save(employee.getPerson()));
		}
		
		//Validtae if position_id is not null and if it exist on DB
		Optional<Position> position = positionRepository.findById(employee.getPosition().getId());
		
		//If exist, we ignore the info sent on the request and use the DB data
		if(position.isPresent()) {
			employee.setPosition(position.get());
		}else {
			Util.isEmpty(employeeDto.getPosition().getName(), "position.name");
			//Else, we create a new Position
			Position positionNew = new Position();
			positionNew.setName(employeeDto.getPosition().getName());
			employee.setPosition(positionRepository.save(positionNew));
		}
		
		employee = emploRepository.save(employee);
		return employeeMapper.convertToDto(employee);
	}

	@Override
	public EmployeeDTO updateEmployee(int id, EmployeeDTO employeeDto) throws Exception {
		
		Optional<Employee> employeeDB = emploRepository.findById(id);
		if(!employeeDB.isPresent()) {
			throw new Error("Employee no exist");
		}
		
		Employee employee = employeeDB.get();
		
		Optional<Person> person = personRepository.findById(employeeDto.getPerson().getId());
		if(person.isPresent()) {
			//If exist, we ignore the info sent on the request and use the DB data
			employee.setPerson(person.get());
		}else {
			employee.getPerson().setName(ObjectUtils.isEmpty(employeeDto.getPerson().getName())?employee.getPerson().getName():employeeDto.getPerson().getName());
			employee.getPerson().setLastName(ObjectUtils.isEmpty(employeeDto.getPerson().getLastName())?employee.getPerson().getLastName():employeeDto.getPerson().getLastName());
			employee.getPerson().setAddress(ObjectUtils.isEmpty(employeeDto.getPerson().getAddress())?employee.getPerson().getAddress():employeeDto.getPerson().getAddress());
			employee.getPerson().setCellphone(ObjectUtils.isEmpty(employeeDto.getPerson().getCellphone())?employee.getPerson().getCellphone():employeeDto.getPerson().getCellphone());
			employee.getPerson().setCityName(ObjectUtils.isEmpty(employeeDto.getPerson().getCityName())?employee.getPerson().getCityName():employeeDto.getPerson().getCityName());	
		}	
		
		//Validtae if position_id is not null and if it exist on DB
		Optional<Position> position = positionRepository.findById(employeeDto.getPosition().getId());
		
		//If exist, we ignore the info sent on the request and use the DB data
		if(position.isPresent()) {
			employee.setPosition(position.get());
		}else {
			Util.isEmpty(employeeDto.getPosition().getName(), "position.name");
			//Else, we create a new Position
			Position positionNew = new Position();
			positionNew.setName(employeeDto.getPosition().getName());
			employee.setPosition(positionRepository.save(positionNew));
		}
		
		employee = emploRepository.save(employee);
		return employeeMapper.convertToDto(employee);
	}

	@Override
	public void deleteEmployee(int id) {
		emploRepository.deleteById(id);
	}
}

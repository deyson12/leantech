package com.leantech.project.service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.leantech.project.dto.EmployeeByPositionDTO;
import com.leantech.project.dto.EmployeeDTO;
import com.leantech.project.dto.PositionListDTO;
import com.leantech.project.mapper.EmployeeMapper;
import com.leantech.project.model.Employee;
import com.leantech.project.model.Position;
import com.leantech.project.repository.IEmployeeRepository;
import com.leantech.project.repository.IPositionRepository;

@Service
public class PositionService implements IPositionService{
	
	@PersistenceContext
	private EntityManager entityManager;
	
	@Autowired
	private EmployeeMapper employeeMapper;

	@Autowired
	private IPositionRepository positionRepository;
	
	@Autowired
	private IEmployeeRepository employeeRepository;
	

	@Override
	public List<PositionListDTO> listPositionwithEmployees() {
		
		List<PositionListDTO> list = new ArrayList<PositionListDTO>();
		for(Position position : positionRepository.findAll()) {
			
			
			CriteriaBuilder cb = entityManager.getCriteriaBuilder();
	        CriteriaQuery<Employee> cq = cb.createQuery(Employee.class);
	        Root<Employee> rootEntry = cq.from(Employee.class);
			Predicate predicate = cb.equal(rootEntry.get("position").get("id"), position.getId());
	        CriteriaQuery<Employee> all = cq.select(rootEntry)
	        		.where(predicate)
	        		.orderBy(cb.desc(rootEntry.get("salary")));
	        
	        TypedQuery<Employee> allQuery = entityManager.createQuery(all);
	        
	        List<EmployeeByPositionDTO> listEmployees = allQuery.getResultList().stream().map(employee -> {
						return employeeMapper.convertToDtoByPosition((employee));
					}).collect(Collectors.toList());
			
			PositionListDTO positionListDto = new PositionListDTO();
			positionListDto.setId(position.getId());
			positionListDto.setName(position.getName());
			positionListDto.setEmployees(listEmployees);
			
			list.add(positionListDto);	
		}
		 
		
		return list;
	}
	
    private Sort sortBySalaryDesc() {
        return Sort.by(Sort.Direction.DESC, "salary");
    }
}

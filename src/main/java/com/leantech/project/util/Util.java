package com.leantech.project.util;

import org.springframework.util.ObjectUtils;

public class Util {
	
	public static void isEmpty(Object obj, String varName) throws Exception{
		if(ObjectUtils.isEmpty(obj)) {
			throw new Exception(varName + " is required");
		}
	}

}

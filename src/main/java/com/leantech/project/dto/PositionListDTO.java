package com.leantech.project.dto;

import java.util.List;

public class PositionListDTO {
	
	private int id;
	private String name;
	private List<EmployeeByPositionDTO> employees;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<EmployeeByPositionDTO> getEmployees() {
		return employees;
	}

	public void setEmployees(List<EmployeeByPositionDTO> employees) {
		this.employees = employees;
	}


}

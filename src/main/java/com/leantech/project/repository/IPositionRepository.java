package com.leantech.project.repository;

import org.springframework.data.repository.CrudRepository;

import com.leantech.project.model.Position;

	public interface IPositionRepository extends CrudRepository<Position, Integer>{

}

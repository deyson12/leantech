package com.leantech.project.repository;

import org.springframework.data.repository.CrudRepository;

import com.leantech.project.model.Person;

	public interface IPersonRepository extends CrudRepository<Person, Integer>{

}

package com.leantech.project.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.leantech.project.model.Employee;

	public interface IEmployeeRepository extends JpaRepository<Employee, Integer>{

}

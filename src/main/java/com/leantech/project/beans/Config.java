package com.leantech.project.beans;

import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.leantech.project.mapper.EmployeeMapper;

@Configuration
public class Config {
	
	@Bean
	public EmployeeMapper newEmployMapper() {
		return new EmployeeMapper();
	}
	
	@Bean
	public ModelMapper modelMapper() {
	    return new ModelMapper();
	}

}

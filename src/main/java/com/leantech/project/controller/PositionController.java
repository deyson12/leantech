package com.leantech.project.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.leantech.project.dto.PositionListDTO;
import com.leantech.project.service.IPositionService;

@RestController
public class PositionController {
	
	@Autowired
	private IPositionService positionService;
	
	/**
	 * Listar todos los empleados (Este Endpoint debe permitir filtrar por cargo o nombre) estos parámetros son opcionales, 
	 * si no se envía alguno de estos, debe traer todos los empleados.
	 * @param name
	 * @param position
	 * @return
	 */
	@GetMapping("/position")
	public ResponseEntity<List<PositionListDTO>> listEmployees() {
		
		List<PositionListDTO> listPositionWithEmployees = positionService.listPositionwithEmployees();
		
		return new ResponseEntity<List<PositionListDTO>>(listPositionWithEmployees, HttpStatus.OK);
	}
	
}

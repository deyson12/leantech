package com.leantech.project.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.leantech.project.dto.EmployeeDTO;
import com.leantech.project.dto.ResponseDTO;
import com.leantech.project.service.IEmployeeService;

@RestController
public class EmployeeController {
	
	@Autowired
	private IEmployeeService employeeService;
	
	/**
	 * Listar todos los empleados (Este Endpoint debe permitir filtrar por cargo o nombre) estos parámetros son opcionales, 
	 * si no se envía alguno de estos, debe traer todos los empleados.
	 * @param name
	 * @param position
	 * @return
	 */
	@GetMapping("/employee")
	public ResponseEntity<List<EmployeeDTO>> listEmployees(@RequestParam(required = false) String name, @RequestParam(required = false) String position) {
		
		List<EmployeeDTO> listEmployees = employeeService.listEmployees(name, position);
		
		return new ResponseEntity<List<EmployeeDTO>>(listEmployees, HttpStatus.OK);
	}
	
	@PostMapping("/employee")
	public ResponseEntity<?> createEmployee(@RequestBody EmployeeDTO employee) {
		try {
			return new ResponseEntity<EmployeeDTO>(employeeService.createEmployee(employee), HttpStatus.OK);
		}catch (Exception e) {
			return new ResponseEntity<ResponseDTO>(new ResponseDTO(e.getMessage()), HttpStatus.BAD_REQUEST);
		}
		
	}
	
	@PutMapping("/employee/{id}")
	public ResponseEntity<?> updateEmployee(@PathVariable int id, @RequestBody EmployeeDTO employee) {
		
		try {
			return new ResponseEntity<EmployeeDTO>(employeeService.updateEmployee(id, employee), HttpStatus.OK);
		}catch (Exception e) {
			return new ResponseEntity<ResponseDTO>(new ResponseDTO(e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@DeleteMapping("/employee/{id}")
	public ResponseEntity<ResponseDTO> deleteEmployee(@PathVariable int id){
		try {
			employeeService.deleteEmployee(id);
			return new ResponseEntity<ResponseDTO>(new ResponseDTO("Delete successful"), HttpStatus.OK);
		}catch (Exception e) {
			return new ResponseEntity<ResponseDTO>(new ResponseDTO("Error trying to delete employe"), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

}

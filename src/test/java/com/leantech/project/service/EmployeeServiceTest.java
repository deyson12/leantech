package com.leantech.project.service;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.times;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.util.ReflectionTestUtils;

import com.leantech.project.dto.EmployeeDTO;
import com.leantech.project.mapper.EmployeeMapper;
import com.leantech.project.model.Employee;

public class EmployeeServiceTest {
	
	EmployeeService employeeService;
	
	@Mock
	private Query query;
	
	private EntityManager entityManager;
	private EmployeeMapper employeeMapper;
	
	@Mock
	private Employee employee;
	@Mock
	private EmployeeDTO employeeDTO;
	
    @BeforeEach                                         
    void setUp() {
    	employeeService = new EmployeeService();
    	entityManager = mock(EntityManager.class);
    	query = mock(Query.class);
    	employeeMapper = mock(EmployeeMapper.class);
    	employeeDTO = mock(EmployeeDTO.class);
    }
	
	@Test
	public void listEmployees() {
		
		ReflectionTestUtils.setField(employeeService, "entityManager", entityManager);
		ReflectionTestUtils.setField(employeeService, "employeeMapper", employeeMapper);
		
		String queryString = "SELECT e FROM Employee e WHERE 1=1 AND  upper(e.person.name) LIKE upper(:name) AND  upper(e.position.name) LIKE upper(:position) ";
	    when(entityManager.createQuery(queryString)).thenReturn(query);	    
	    
	    List<Employee> emloyees = new ArrayList<Employee>();
	    emloyees.add(employee);
	    
	    when(query.getResultList()).thenReturn(emloyees);
	    when(employeeMapper.convertToDto(employee)).thenReturn(employeeDTO);
	    when(employeeDTO.getSalary()).thenReturn(1000);
		
		List<EmployeeDTO> listEmployees = employeeService.listEmployees("Dey", "Dev");
		
		System.out.println(query.getParameter("name"));
		
		verify(query, times(2)).setParameter(Mockito.anyString(), Mockito.anyString());
		assertTrue(listEmployees.size()>0);
		assertEquals(1000, listEmployees.get(0).getSalary());
	}

}

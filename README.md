<div  align="center">

<a  href="http://deysonestrada.com"><img  width="100%"  src="https://raw.githubusercontent.com/deyson12/deyson12/master/github.png"  alt="cover"  /></a>

</div>
## Hola, Soy Deyson Estrada and ❤️ code
<div>

<a  href="https://www.linkedin.com/in/dfep/"  target="_blank">

<img src="https://img.shields.io/badge/LinkedIn-%230077B5.svg?&style=flat-square&logo=linkedin&logoColor=white&color=071A2C" alt="LinkedIn">
</a>
<a  href="https://www.instagram.com/deysonestrad/"  target="_blank">

<img src="https://img.shields.io/badge/Instagram-%23E4405F.svg?&style=flat-square&logo=instagram&logoColor=white&color=071A2C" alt="Instagram">

</a>

<a  href="mailto:deyson12@gmail.com"  mailto="deyson12@gmail.com"  target="_blank">

<img src="https://img.shields.io/badge/Gmail-%231877F2.svg?&style=flat-square&logo=gmail&logoColor=white&color=071A2C" alt="Gmail">

</a>

<a  href="http://deysonestrada.com"  target="_blank">

<img src="https://img.shields.io/badge/Freelancer-%231877F2.svg?&style=flat-square&logo=freelancer&logoColor=white&color=071A2C" alt="Freelancer">

</a>

</div>

<br  />
<h2>Colección de POSTMAN para ejecutar el proyecto</h2>

[![Run in Postman](https://run.pstmn.io/button.svg)](https://app.getpostman.com/run-collection/aca2198503854a25ff3e)
  En caso de no funcionar, dar click [AQUI](https://www.getpostman.com/collections/aca2198503854a25ff3e)
  
  ***Endpoints de ejemplo para Empleados:***
  <br />
  **Get** localhost:8080/employee?position=DeV&name=DeY
  <br />
  **Post** localhost:8080/employee (Agregar Body)
  <br />
  **Delete** localhost:8080/employee/1
  <br />
  **Put** localhost:8080/employee/1 (Agregar Body)
  
  ***Enpoint de ejemplo para Cargo/Posición***
  <br />
  **Get** localhost:8080/position
  

More in [deysonestrada.com](http://deysonestrada.com)